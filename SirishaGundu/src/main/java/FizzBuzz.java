import java.util.Calendar;

public class FizzBuzz {
	
	public static void main(String[] args) {
		FizzBuzz fizzBuzz = new FizzBuzz();
		fizzBuzz.wedCheck();
		
	}
	
	public void wedCheck()
	{
		FizzBuzz fizzBuzz = new FizzBuzz();
		Calendar calendar = Calendar.getInstance();
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if(dayOfWeek == 3)
			fizzBuzz.getFizzAndBuzz(21);
		else
			fizzBuzz.getFizzAndBuzzWedDay(21);
		
	}

	public void getFizzAndBuzz(int num) {
		for (int i = 1; i < num; i++) {
			
			if (i % 3 == 0) {
				System.out.println("fizz");

			} else if (i % 5 == 0) {
				System.out.println("buzz");

			} else if (i % 3 == 0 && i % 5 == 0) {
				System.out.println("fizz buzz");
			} else {
				System.out.println(i);
			}
		}

	}
	
	public void getFizzAndBuzzWedDay(int num) {

		for (int i = 1; i < num; i++) {
			
			if (i % 3 == 0) {
				System.out.println("wizz");

			} else if (i % 5 == 0) {
				System.out.println("wuzz");

			} else if (i % 3 == 0 && i % 5 == 0) {
				System.out.println("wizz wuzz");
			} else {
				System.out.println(i);
			}
		}

	}
}
