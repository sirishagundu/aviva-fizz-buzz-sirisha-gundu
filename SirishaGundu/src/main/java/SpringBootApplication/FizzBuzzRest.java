package SpringBootApplication;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class FizzBuzzRest {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(FizzBuzzRest.class, args);
	}

	@RequestMapping("/api")
	String home() {
		return "Hello!";
	}

	@RequestMapping(value = "/FizzBuzz", method = RequestMethod.GET)
	public List<Object> getFizzBuzz(@RequestParam String num) {
		List<Object> numberList = new ArrayList<Object>();
		int num1 = Integer.parseUnsignedInt(num);

		for (int i = 1; i < num1; i++) {
			if (i % 3 == 0) {
				numberList.add(i, "fizz");
			} else if (i % 5 == 0) {
				numberList.add(i, "buzz");
			} else if (i % 3 == 0 && i % 5 == 0) {
				System.out.println("fizz buzz");
				numberList.add(i, "fizz buzz");
			} else {
				System.out.println(i);
				numberList.add(i, i);
			}

		}
		return numberList;
	}
	
	@RequestMapping(value = "/FizzBuzzDay", method = RequestMethod.GET)
	public List<Object> getFizzBuzzDay(@RequestParam int num) {
		List<Object> numberList = new ArrayList<Object>();
		Calendar calendar = Calendar.getInstance();
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if(dayOfWeek!=3)
		{
		for (int i = 1; i < num; i++) {			
			if (i % 3 == 0) {
				numberList.add(i, "fizz");
			} else if (i % 5 == 0) {
				numberList.add(i, "buzz");
			} else if (i % 3 == 0 && i % 5 == 0) {
				System.out.println("fizz buzz");
				numberList.add(i, "fizz buzz");
			} else {
				System.out.println(i);
				numberList.add(i, i);
			}

		}
	}
		else
		{ 
			for (int i = 1; i < num; i++) {			
				if (i % 3 == 0) {
					numberList.add(i, "wizz");
				} else if (i % 5 == 0) {
					numberList.add(i, "wuzz");
				} else if (i % 3 == 0 && i % 5 == 0) {
					System.out.println("wizz wuzz");
					numberList.add(i, "wizz wuzz");
				} else {
					System.out.println(i);
					numberList.add(i, i);
				}

			}
		}
		return numberList;
	}
}
